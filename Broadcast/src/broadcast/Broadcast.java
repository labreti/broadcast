package broadcast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Broadcast {
	public static void main(String[] args) throws IOException {
	        String msg="ciao\n";
	        int port=50000;
	        InetAddress broadcastAddress = InetAddress.getByName("192.168.5.255");
	        DatagramSocket socket = new DatagramSocket();
	        byte[] buf = new byte[256];
	        buf = msg.getBytes();
	        DatagramPacket packet = new DatagramPacket(buf, buf.length, broadcastAddress,port);
	        socket.send(packet);
	        socket.close();
	}

}
