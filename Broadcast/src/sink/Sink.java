package sink;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Sink {
	static int port=50000;
	public static void main(String[] args) throws IOException {
		DatagramSocket socket = new DatagramSocket(port);
		DatagramPacket packet = new DatagramPacket(new byte[256], 256);
		try  {
			while ( true ) {
				socket.receive(packet);
				String received = new String(packet.getData(), 0, packet.getLength());
				System.out.println(received);
			} 
		} finally {
			socket.close();
		}
	}
}